let requestPerson = "../data/person.json";
let requestPoints = "../data/points.json";
let requestProducts = "../data/product.json";
let section = document.getElementById("wrapper")
let data;
let table;

function getJSON() {
  let url = document.getElementById("wrapper").getAttribute("data-source");
  fetch(url) // j'envoie la requête
    .then(   // quand je reçois la réponse
      response => response.json()
    ) // je récupère le JSON de la réponse
    .then(
        // quand c'est pret
        json_data => {
        data = json_data;
        // je mets mes data dans ma variable
          display_HTML()
      }
    )
    .catch(err => console.log(err)) // affiche les erreurs HTTP au cas où
}

function updateData(obj_index, key, value) {
  data[obj_index][key] = value;
}

let personJSON = document.getElementById("person");
personJSON.addEventListener("click", function (e) {
  fetch(requestPerson)
      .then(
      rep => rep.json()
    )
      .then(
        json_data => {
          data = json_data;
          display_HTML()
      }
    )
      .catch(err => console.log(err))
    section.innerHTML = ""
});

let pointsJSON = document.getElementById("points");
pointsJSON.addEventListener("click", function (e) {
  fetch(requestPoints)
      .then(
      rep => rep.json()
    )
      .then(
        json_data => {
          data = json_data;
          display_HTML()
      }
    )
      .catch(err => console.log(err))
    section.innerHTML = ""
});

let productsJSON = document.getElementById("product");
productsJSON.addEventListener("click", function (e) {
  fetch(requestProducts)
      .then(
      rep => rep.json()
    )
      .then(
        json_data => {
          data = json_data;
          display_HTML()
      }
    )
      .catch(err => console.log(err))
    section.innerHTML = ""
});

function display_HTML() {
  table = document.createElement('table');
  table.setAttribute('id', 'myTable')
  let container = document.getElementById("wrapper")
  container.appendChild(table)
  display_tableHead()
  display_tableBody()
}

function display_tableHead() {
  let tableHeader = document.createElement('thead')
  let tr = document.createElement('tr')
  let obj = data[0]
  for (const key in obj) {
    let th = document.createElement('th')
    th.innerText = key;
    th.addEventListener('click', trier)
    tr.appendChild(th)
  }
  tableHeader.appendChild(tr)
  table.appendChild(tableHeader)
}

function display_tableBody() {
  let tableBody = document.createElement('tbody')
  //Permet de récuperer l'odre des produits dans la table
  let indexOnTable = 0
  for (const obj of data) {
    let tr = document.createElement('tr')
    tr.classList.add("ligne")
    for (const key in obj) {
      let td = document.createElement('td')
      td.innerText = obj[key];
      td.contentEditable = "true"
      td.classList.add('cell')
      td.setAttribute('data-key', key)
      td.setAttribute('data-index', indexOnTable)
      td.addEventListener('input', modifCell)
      tr.appendChild(td)
    }
    tableBody.appendChild(tr)
    indexOnTable++
  }
  let oldBody = table.getElementsByTagName('tbody')[0]
  if (oldBody !== undefined) {
    oldBody.remove();
    }
  table.appendChild(tableBody)
}

//Permet de rechercher un caractère parmis chaque td de tr selon l'input value de la searchBar
function searchTable() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("input-search");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = document.getElementsByClassName("ligne");
  for (i = 0; i < tr.length; i++) {
    td = tr[i];
    if (td) {
      txtValue = td.textContent;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}

document.getElementById('input-search').addEventListener('change', searchTable)

function trier(e) {
  let key = e.target.textContent;
  data.sort((a, b) => {
    if (a[key].toString().toLowerCase() < b[key].toString().toLowerCase()) {
      return -1;
    }
    if (a[key].toString().toLowerCase() > b[key].toString().toLowerCase()) {
      return 1;
    }
    return 0;
  })
  display_tableBody()
}


function ajouter(e) {
  let newObj = {}
  for (const key in data[0]) {
    newObj[key] = ""
  }
  data.push(newObj)
  display_tableBody()
}
document.getElementById('btn-add').addEventListener('click', ajouter)

function modifCell(e) {
  let value = e.target.innerText
  let key = e.target.getAttribute('data-key')
  let obj_index = e.target.getAttribute('data-index')
  updateData(obj_index, key, value)
}


function save(e) {
  // transforme des données en text
  let jsonData = JSON.stringify(data)
  fetch('up.php', {
    method: 'POST',
    headers: {
      "content-type": "application/json"
    },
    body: jsonData
  })
    .then(
    response => response.json()
  )
    .then(
    err => console.log(err)
  )
}

document.getElementById('btn-save').addEventListener('click', save)

//Permet de modifier l'url data-source de la table pour afficher les fichiers json que l'on veut
function searchJSON(e){
  console.log(e.target.value)
  let newJson = e.target.value
  section.setAttribute('data-source', newJson)
  section.innerHTML = ""
  getJSON()
}

document.getElementById('search-json').addEventListener('change', searchJSON)

getJSON()